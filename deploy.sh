#!/usr/bin/env bash

targetDir=/root/watchdominion.ch

# Stop old deployment
ssh root@${PUBLISH_SSH_HOST} "test -d ${targetDir} && cd ${targetDir} && docker-compose stop; \
  test -f ${targetDir}/config/traefik/acme.json && cp ${targetDir}/config/traefik/acme.json /tmp/acme.json; \
  rm -rf ${targetDir}"

# Overall
ssh root@${PUBLISH_SSH_HOST} "mkdir -p ${targetDir}/config"
scp docker-compose.yml root@${PUBLISH_SSH_HOST}:${targetDir}/docker-compose.yml
scp config/${STAGE}.env root@${PUBLISH_SSH_HOST}:${targetDir}/.env

# Traefik
ssh root@${PUBLISH_SSH_HOST} "mkdir -p ${targetDir}/config/traefik"
scp config/traefik/* root@${PUBLISH_SSH_HOST}:${targetDir}/config/traefik
ssh root@${PUBLISH_SSH_HOST} "test -f ${targetDir}/config/traefik/acme.json \
  && mv /tmp/acme.json ${targetDir}/config/traefik/acme.json \
  && touch ${targetDir}/config/traefik/acme.json \
  && chmod 600 ${targetDir}/config/traefik/acme.json"

# Start
ssh root@${PUBLISH_SSH_HOST} "cd ${targetDir} \
    && docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} \
    && docker-compose pull \
    && docker-compose up -d"
